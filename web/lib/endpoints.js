import axios from 'axios'

let server = axios.create({
    baseURL: process.env.SERVER_URL,
    validateStatus: () => true,
})

server.interceptors.request.use(
    (config) => {
        let token = localStorage.getItem('token')

        if (token) config.headers.common['Authorization'] = 'Bearer ' + token
        return config
    },
    (error) => {
        Promise.reject(error)
    }
)

export { server }
