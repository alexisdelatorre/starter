import Title from 'antd/lib/typography/Title'
import { Card, Form, Input, Button, Alert } from 'antd'
import Link from 'next/link'
import { useState } from 'react'
import axios from 'axios'
import { server } from '../lib/endpoints'

function Registration() {
    let [success, setSuccess] = useState(false)
    let [error, setError] = useState(null)
    let [loading, setLoading] = useState(false)

    let handleForm = (values) => {
        if (values.password !== values.repassword) {
            setError('The passwords do not match')
            return
        }

        setLoading(true)
        console.log(values)

        let payload = { user: { email: values.email, password: values.password } }

        server
            .post('/users', payload)
            .then((res) => {
                if (res.status === 200) {
                    setError(null)
                    setLoading(false)
                    localStorage.setItem('token', res.data?.access_token)
                    router.push('/')
                    return
                }

                if (res.status === 201) {
                    setError(null)
                    setSuccess(true)
                    setLoading(false)
                    return
                }

                if (res.status === 500) {
                    console.error(res)
                    setError('Server error, try again later')
                    setLoading(false)
                    return
                }

                console.error(res)
                setError('There was a unknown error')
                setLoading(false)
            })
            .catch((err) => {
                setLoading(false)
                setError(err.message)
            })
    }

    return (
        <div className={'flex justify-center items-center h-screen bg-gray-200'}>
            <div className={'container max-w-lg'}>
                <div className={'text-center'}>
                    <Title>Vende-ML</Title>
                </div>
                <div className={'shadow-lg'}>
                    <Card title='Registration' style={{ minWidth: 450 }}>
                        {success ? (
                            <>
                                User was created succesfully, now you can{' '}
                                <Link href='/login'>
                                    <a>Login</a>
                                </Link>
                            </>
                        ) : (
                            <Form layout='vertical' onFinish={handleForm} hideRequiredMark>
                                {error && <Alert type='error' message={error} />}
                                <Form.Item label='Email' name='email' rules={[{ required: true }]}>
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label='Password'
                                    name='password'
                                    rules={[{ required: true }]}
                                >
                                    <Input.Password />
                                </Form.Item>
                                <Form.Item
                                    label={'Repeat Password'}
                                    name={'repassword'}
                                    rules={[{ required: true }]}
                                >
                                    <Input.Password />
                                </Form.Item>
                                <Form.Item>
                                    <Button
                                        block
                                        type={'primary'}
                                        htmlType={'submit'}
                                        loading={loading}
                                    >
                                        Create
                                    </Button>
                                    <Link href='/login'>
                                        <span>
                                            <Button type={'link'} block>
                                                Or Login
                                            </Button>
                                        </span>
                                    </Link>
                                </Form.Item>
                            </Form>
                        )}
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default Registration
