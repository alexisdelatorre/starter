import { PageHeader } from 'antd'
import { useRouter } from 'next/router'

function Home() {
    let router = useRouter()

    return (
        <div className='container mx-auto mt-10'>
            <div className=''>
                <PageHeader
                    className='border border-solid border-antd-gray-100'
                    title='Home'
                    subTitle='This is a subtitle'
                    onBack={() => router.push('/login')}
                />
            </div>
        </div>
    )
}

export default Home
