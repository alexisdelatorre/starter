import '../styles.less'

import { useRouter } from 'next/router'
import { Menu, Badge, Dropdown } from 'antd'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import SettingsIcon from '@material-ui/icons/Settings'
import EqualizerIcon from '@material-ui/icons/Equalizer'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import { useState, useEffect } from 'react'
import { server } from '../lib/endpoints'

export const Context = React.createContext()

export default function MyApp({ Component, pageProps }) {
    let router = useRouter()

    let [user, setUser] = useState(null)

    let isPublicPage = ['/login', '/registration'].includes(router.pathname)

    useEffect(() => {
        if (isPublicPage && localStorage.getItem('token')) router.push('/')
    })

    useEffect(() => {
        if (isPublicPage && user !== null) router.push('/')
        if (!isPublicPage && user === null) router.push('/login')
    }, [user])

    useEffect(() => {
        server.get('/users/me').then((res) => {
            if (res.status === 200) {
                setUser(res.data)
                return
            }

            if (res.status === 401) {
                setUser(null)
                localStorage.removeItem('token')
                return
            }
        })
    }, [])

    let handleUserMenuClick = ({ key }) => {
        if (key === 'logout') {
            localStorage.removeItem('token')
            router.push('/login')
        }
    }

    if (isPublicPage) {
        return (
            <Context.Provider value={{ user }}>
                <Component {...pageProps} />
            </Context.Provider>
        )
    } else {
        return (
            <Context.Provider value={{ user }}>
                <div className={'flex flex-col h-screen'}>
                    <div className={'py-1 px-6 shadow-md flex items-center justify-between'}>
                        <div className={'font-semibold'}>Logo</div>
                        <div>
                            <Dropdown
                                overlay={
                                    <Menu onClick={handleUserMenuClick}>
                                        <Menu.Item key='logout'>Log Out</Menu.Item>
                                    </Menu>
                                }
                                placement='bottomRight'
                                trigger={['click']}
                            >
                                <AccountCircleIcon className='mr-5 cursor-pointer' />
                            </Dropdown>
                        </div>
                    </div>
                    <div className={'flex-1 flex'}>
                        <Menu
                            style={{ width: 200 }}
                            defaultSelectedKeys={['b']}
                            mode={'inline'}
                            theme={'dark'}
                        >
                            <Menu.Item key={'a'}>
                                <div className={'flex items-center'}>
                                    <EqualizerIcon fontSize={'small'} className={'mr-3'} />
                                    <span>Menu 1</span>
                                </div>
                            </Menu.Item>
                            <Menu.Item key={'b'}>
                                <div className={'flex items-center'}>
                                    <AttachMoneyIcon fontSize={'small'} className={'mr-3'} />
                                    <span>Menu 2</span>
                                </div>
                            </Menu.Item>
                            <Menu.Item key={'c'}>
                                <div className={'flex items-center justify-between'}>
                                    <div className='flex items-center'>
                                        <LocationOnIcon fontSize={'small'} className={'mr-3'} />
                                        <span className={'mr-3'}>Menu 3</span>
                                    </div>
                                    <Badge count={25} />
                                </div>
                            </Menu.Item>
                            <Menu.Item key={'d'}>
                                <div className={'flex items-center'}>
                                    <SettingsIcon fontSize={'small'} className={'mr-3'} />
                                    <span>Menu 4</span>
                                </div>
                            </Menu.Item>
                        </Menu>
                        <div className={'flex-1'}>
                            <Component {...pageProps} />
                        </div>
                    </div>
                </div>
            </Context.Provider>
        )
    }
}
