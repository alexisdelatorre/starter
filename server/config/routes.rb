Rails.application.routes.draw do
  use_doorkeeper
  get 'users/me', to: 'users#me'
  resources :users, except: :index
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
